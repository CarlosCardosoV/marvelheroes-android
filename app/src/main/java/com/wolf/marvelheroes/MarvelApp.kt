package com.wolf.marvelheroes

import android.app.Application
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker
import com.wolf.marvelheroes.di.repositoryModule
import com.wolf.marvelheroes.di.viewmodelModule
import com.wolf.marvelheroes.util.ResourcesManager
import org.koin.android.ext.android.startKoin
import timber.log.Timber

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:23 PM
 */
class MarvelApp : Application() {

    override fun onCreate() {
        super.onCreate()

        InternetAvailabilityChecker.init(this)

        ResourcesManager.init(applicationContext)

        /** Timber */
        Timber.plant(Timber.DebugTree())

        /** di  */
        startKoin(this, listOf(viewmodelModule, repositoryModule))

    }
}