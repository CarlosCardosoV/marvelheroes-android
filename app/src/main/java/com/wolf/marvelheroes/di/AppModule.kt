package com.wolf.marvelheroes.di

import com.wolf.marvelheroes.model.repository.HerosRepository
import com.wolf.marvelheroes.model.repository.NetworkHeroRepository
import com.wolf.marvelheroes.ui.home.HomeViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:46 PM
 *
 * Definiton of all dependencies: viewmodels will be provided with their repository
 * and will be injected in the activities
 */
val viewmodelModule = module {
    viewModel { HomeViewModel(get()) }
}

val repositoryModule = module {
    single<HerosRepository> { NetworkHeroRepository() }
}