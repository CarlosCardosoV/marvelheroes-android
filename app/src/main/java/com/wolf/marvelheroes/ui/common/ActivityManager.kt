package com.wolf.marvelheroes.ui.common

import android.content.Context
import android.support.v4.content.ContextCompat
import com.tfb.fbtoast.FBCustomToast
import com.wolf.marvelheroes.R

/**
 * @author carloscardoso
 * Date 6/17/19 at 11:52 PM
 *
 * Class with util methods associated to activities and views
 */
object ActivityManager {

    fun showToast(context: Context, message: String){
        val fbCustomToast = FBCustomToast(context)
        fbCustomToast.setMsg(message)
        fbCustomToast.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bg_button))
        fbCustomToast.show()
    }


}