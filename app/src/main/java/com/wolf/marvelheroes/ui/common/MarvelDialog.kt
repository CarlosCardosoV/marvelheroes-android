package com.wolf.marvelheroes.ui.common

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.wolf.marvelheroes.R

/**
 * @author carloscardoso
 * Date 6/18/19 at 12:05 AM
 *
 * A custom dialogs impl
 */
class MarvelDialog(context: Context?, activity: Activity) : Dialog(context) {

    var activity: Activity

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.activity = activity
    }


    fun showNoInternetDialog() {

        window?.setBackgroundDrawable(
                ContextCompat.getDrawable(context, R.drawable.bg_transparent))
        setContentView(R.layout.dialog_no_internet)

        val lavInternet: LottieAnimationView = findViewById(R.id.lavInternet)
        lavInternet.playAnimation()

        setCancelable(true)
        try {
            show()
        } catch (e: Exception) {
        }

    }



}