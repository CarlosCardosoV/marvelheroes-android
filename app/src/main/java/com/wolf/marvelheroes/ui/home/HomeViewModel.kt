package com.wolf.marvelheroes.ui.home

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.wolf.marvelheroes.R
import com.wolf.marvelheroes.model.network.heros.Superhero
import com.wolf.marvelheroes.model.repository.HerosRepository
import com.wolf.marvelheroes.ui.common.BaseViewModel
import com.wolf.marvelheroes.util.ResourcesManager

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:36 PM
 */
class HomeViewModel(private val herosRepository: HerosRepository) : BaseViewModel() {

    val _heros: MutableLiveData<List<Superhero>> = MutableLiveData()
    val heros: LiveData<List<Superhero>> get() = _heros

    fun getHeros(){
        _loading.value = true

        herosRepository.getHeros {
            _loading.value = false

            if (it != null) _heros.value = it else _message.value = ResourcesManager.getString(R.string.home_heroe_consume_error)
        }
    }
}