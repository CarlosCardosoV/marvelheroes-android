package com.wolf.marvelheroes.ui.home

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.wolf.marvelheroes.R
import com.wolf.marvelheroes.model.network.heros.Superhero
import com.wolf.marvelheroes.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loader.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private val viewmodel: HomeViewModel by viewModel()
    private var adapter: HeroAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewmodel.getHeros()

        /** Actions */
        observeViewModel()

        swipRefresh.setOnRefreshListener {
            viewmodel.getHeros()
        }
    }

    private fun observeViewModel(){
        viewmodel.message.observe(this, messageObserver(this))
        viewmodel.loading.observe(this, loadingObserver(loader.lavLoader))
        viewmodel.heros.observe(this, Observer { if (it != null) setHeros(it) })
    }

    private fun setHeros(heros: List<Superhero>){
        swipRefresh.isRefreshing = false

        /** Show empty section  if needed */
        if (heros.isEmpty()) lavEmptySection.playAnimation() else lavEmptySection.pauseAnimation()
        val emptyVisibility = if (heros.isEmpty()) View.VISIBLE else View.GONE

        tvHeroesEmptySection.visibility = emptyVisibility
        lavEmptySection.visibility = emptyVisibility

        /** Setting heros */
        adapter = HeroAdapter(applicationContext, this, heros)
        rvHeros.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvHeros.adapter = adapter
        adapter?.notifyDataSetChanged()
    }

}
