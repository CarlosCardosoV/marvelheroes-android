package com.wolf.marvelheroes.ui.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.wolf.marvelheroes.R
import com.wolf.marvelheroes.model.network.heros.Superhero
import com.wolf.marvelheroes.ui.detail.HeroDetailActivity

/**
 * @author carloscardoso
 * Date 6/17/19 at 9:20 PM
 */
class HeroAdapter(
        private val context: Context,
        private val activity: Activity,
        private val heros: List<Superhero>
) : RecyclerView.Adapter<HeroAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_hero,parent, false))
    }

    override fun getItemCount() = heros.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hero = heros[position]

        holder.tvHeroName.text = hero.name
        holder.tvHeroRealName.text = hero.realName

        /** Set data */
        Glide.with(activity)
                .load(hero.photo)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
                .into(holder.ivHero)


        /** Actions */

        holder.btnLoadMore.setOnClickListener {
            val intent = Intent(context, HeroDetailActivity::class.java)
            intent.putExtra("hero", hero)
            activity.startActivity(intent)
        }

        val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        holder.itemView.startAnimation(animation)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val ivHero: ImageView = itemView.findViewById(R.id.ivHero)
        val btnLoadMore: Button = itemView.findViewById(R.id.btnLoadContent)

        val tvHeroName: TextView = itemView.findViewById(R.id.tvHeroName)
        val tvHeroRealName: TextView = itemView.findViewById(R.id.tvHeroRealName)
    }

}