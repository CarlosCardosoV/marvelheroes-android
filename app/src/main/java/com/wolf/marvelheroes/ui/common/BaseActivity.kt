package com.wolf.marvelheroes.ui.common

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker
import com.treebo.internetavailabilitychecker.InternetConnectivityListener

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:38 PM
 *
 * Activity with common features,
 * in order to simplify the code in all activities
 */

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), InternetConnectivityListener, BaseView {


    private var noInternetDialog: MarvelDialog? = null

    /** For internet validation */
    private lateinit var mInternetAvailabilityChecker: InternetAvailabilityChecker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance()
        mInternetAvailabilityChecker.addInternetConnectivityListener(this)
    }


    override fun onDestroy() {
        super.onDestroy()

        mInternetAvailabilityChecker
                .removeInternetConnectivityChangeListener(this)

    }

    override fun onInternetConnectivityChanged(isConnected: Boolean) {

        if (isConnected) {
            noInternetDialog?.dismiss()
        } else {
            noInternetDialog = MarvelDialog(this, this)
            noInternetDialog?.showNoInternetDialog()
        }

    }


}