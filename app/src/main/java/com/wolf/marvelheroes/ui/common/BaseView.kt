package com.wolf.marvelheroes.ui.common

import android.arch.lifecycle.Observer
import android.content.Context
import android.view.View

import com.airbnb.lottie.LottieAnimationView

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:38 PM
 */
interface BaseView {

    fun loadingObserver(view: android.view.View) : Observer<Boolean?> = Observer {
        if (view is LottieAnimationView){
            if (it == true) view.playAnimation() else view.pauseAnimation()
            view.visibility = if (it == true) View.VISIBLE else View.GONE
        }
    }

    fun messageObserver(context: Context) : Observer<String?> = Observer {
        if(it != null){ ActivityManager.showToast(context, it) }
    }


}