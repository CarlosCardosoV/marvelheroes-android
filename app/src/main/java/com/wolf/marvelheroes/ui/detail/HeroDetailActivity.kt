package com.wolf.marvelheroes.ui.detail

import android.os.Bundle
import com.bumptech.glide.Glide
import com.jaeger.library.StatusBarUtil
import com.wolf.marvelheroes.R
import com.wolf.marvelheroes.model.network.heros.Superhero
import com.wolf.marvelheroes.ui.common.BaseActivity
import kotlinx.android.synthetic.main.activity_hero_detail.*
import kotlinx.android.synthetic.main.hero_detail_bottom_sheet.view.*
import kotlinx.android.synthetic.main.hero_detail_content.view.*

class HeroDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtil.setTransparent(this)
        setContentView(R.layout.activity_hero_detail)

        val hero = intent.getSerializableExtra("hero") as Superhero
        setHero(hero)
    }


    private fun setHero(hero: Superhero){
        Glide.with(applicationContext)
                .load(hero.photo)
                .into(heroContent.ivHeroImage)

        bottomSheet.tvHeroName.text = hero.name
        bottomSheet.tvHeroRealName.text = getString(R.string.hero_detail_real_name, hero.realName)
        bottomSheet.tvHeroHeight.text = getString(R.string.hero_detail_height, hero.height)
        bottomSheet.tvHeroPowers.text = getString(R.string.hero_detail_powers, hero.power)
        bottomSheet.tvHeroSkills.text = getString(R.string.hero_detail_skills, hero.abilities)
        bottomSheet.tvHeroGroups.text = getString(R.string.hero_detail_groups, hero.groups)
    }
}
