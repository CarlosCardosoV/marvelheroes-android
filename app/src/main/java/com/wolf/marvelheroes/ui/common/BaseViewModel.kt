package com.wolf.marvelheroes.ui.common

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:47 PM
 *
 *  Viewmodel with common features,
 *  in order to simplify the code in all viewmodels
 */
open class BaseViewModel : ViewModel() {

    /* Mutable Live Data */
    val _message: MutableLiveData<String?> = MutableLiveData()
    val _loading: MutableLiveData<Boolean?> = MutableLiveData()

    /* Exposed Live Data */
    val message: LiveData<String?> get() = _message
    val loading: LiveData<Boolean?> get() = _loading
}