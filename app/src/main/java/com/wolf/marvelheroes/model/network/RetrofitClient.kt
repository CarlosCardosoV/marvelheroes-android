package com.wolf.marvelheroes.model.network

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * @author carloscardoso
 * Date 6/17/19 at 6:06 PM
 */
class RetrofitClient {

    private val CONNECT_TIMEOUT: Long = 100
    private val READ_TIMEOUT: Long = 100
    private val TIME_UNIT = TimeUnit.SECONDS

    private val client = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TIME_UNIT)
            .readTimeout(READ_TIMEOUT, TIME_UNIT)
            .build()

    private var retrofit: Retrofit? = null

    fun getClient(baseUrl: String): Retrofit? {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        //Create client with gson and rxandroid integration
        if (retrofit == null){
            retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }

        return retrofit
    }


    /*Common template in order to manage request results */
    interface TemplateCallback<in T>{
        fun onSuccess(result: T?)
        fun onError(errorCode:Int, message:String)
    }
}