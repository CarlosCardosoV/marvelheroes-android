package com.wolf.marvelheroes.model.network.heros

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:34 PM
 */
interface HerosApiService {

    @GET("bins/bvyob")
    fun getHeroes(): Observable<Response<GetHerosResponse>>

}