package com.wolf.marvelheroes.model.repository

import com.wolf.marvelheroes.model.network.RetrofitClient
import com.wolf.marvelheroes.model.network.heros.GetHerosResponse
import com.wolf.marvelheroes.model.network.heros.HerosApiManager
import com.wolf.marvelheroes.model.network.heros.Superhero

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:59 PM
 */
class NetworkHeroRepository : HerosRepository {

    //Call to manager in order to make request
    override fun getHeros(callback: (heroes: List<Superhero>?) -> Unit) {

        HerosApiManager().getHeroes(object: RetrofitClient.TemplateCallback<GetHerosResponse>{
            override fun onSuccess(result: GetHerosResponse?) { callback(result?.superheroes) }
            override fun onError(errorCode: Int, message: String) { callback(null) }
        })

    }
}