package com.wolf.marvelheroes.model.network.heros

import java.io.Serializable

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:33 PM
 */
data class GetHerosResponse(
    val superheroes: List<Superhero>
)

data class Superhero(
    val abilities: String? = null,
    val groups: String? = null,
    val height: String? = null,
    val name: String? = null,
    val photo: String = "",
    val power: String? = null,
    val realName: String? = null
) : Serializable