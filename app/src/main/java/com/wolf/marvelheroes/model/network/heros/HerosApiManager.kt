package com.wolf.marvelheroes.model.network.heros

import com.wolf.marvelheroes.model.network.ApiUtil
import com.wolf.marvelheroes.model.network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:34 PM
 */
class HerosApiManager {

    private val service = RetrofitClient().getClient(ApiUtil.baseUrl)?.create(HerosApiService::class.java)

    //Perform request via retrofit and rxjava

    fun getHeroes(callback: RetrofitClient.TemplateCallback<GetHerosResponse>){
        service?.getHeroes()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ response ->
                    if (response.isSuccessful)  callback.onSuccess(response.body())
                    else callback.onError(response.code(), response.message())

                }, { callback.onError(it.hashCode(),  it.localizedMessage)})
    }
}