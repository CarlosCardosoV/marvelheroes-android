package com.wolf.marvelheroes.model.repository

import com.wolf.marvelheroes.model.network.heros.Superhero

/**
 * @author carloscardoso
 * Date 6/17/19 at 5:37 PM
 */
interface HerosRepository {
    fun getHeros(callback: (heros: List<Superhero>?) -> Unit)
}