package com.wolf.marvelheroes.util

import android.content.Context
import android.content.res.Resources

/**
 * @author carloscardoso
 * Date 6/17/19 at 6:15 PM
 */
object ResourcesManager {

    /* Resources of the application */
    private lateinit var resources: Resources

    /**
     * @param applicationContext
     */
    fun init(applicationContext: Context){
        resources = applicationContext.resources
    }

    /**
     * @param id the id of the resource
     * @return [String] the resource requested
     */
    fun getString(id: Int) : String = resources.getString(id)

}